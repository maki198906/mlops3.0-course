from typing import Dict, List


def sumof2(target: int, lst: List[int]) -> tuple | str:
    """Returns sum of two numbers in list in respect with target

    Args:
        target (int): any number
        lst (list): list of numbers

    Returns:
        tuple: tuple of two numbers
    """
    storage: Dict = {}
    for el in lst:
        compl = target - el
        if storage.get(el):
            return (el, compl)
        storage[compl] = True
    return "not found"


print(sumof2(10, [1, 5, 8, 9, 3]))
